# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [1.0.0](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/compare/v0.3.2...v1.0.0) (2021-03-22)


### ⚠ BREAKING CHANGES

* this modification will force any dependent project to
resave all overridden VIs

### Features

* adds function for removing single key and its value ([#12](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/issues/12)) ([422c899](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/422c8997d51529e3a8e99890f01a71d942c327c9))
* includes new output for type indication ([776690e](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/776690e10824c0f3c13647e9ff2b643ed1239c9a))
* private function for replacing special chars in regex ([1e4eb51](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/1e4eb51376f012259865d75e52a2e458fded76e3))


### Tests

* **unit:** adds test for remove key and value function ([8c17c44](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/8c17c44050bfed2dc64e55ac4c34f32d81048a18))
* **unit:** adds test for returning types in extract value function ([e29b3bb](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/e29b3bb5e54311e5afb299f458902ede37d49916))


### Code Refactoring

* changes object-to-json vis to shared reentrancy ([#11](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/issues/11)) ([a816647](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/a81664744828eecae4f425f459f2d8231a9baa62))

### [0.3.2](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/compare/v0.3.1...v0.3.2) (2021-03-09)


### Features

* deprecates the extract object from JSON ([d2dc226](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/d2dc226feb239ccdc70c38ea94a61ec55c5c77c1))


### Bug Fixes

* extracts value allows nested objects ([94ab724](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/94ab724d09b14e029f7313a64c11d2e1a38bde3d))


### Code Refactoring

* adds condition for adding space after two dots ([9789e88](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/9789e88f984d8b8648055f7e83af37c89ce2e5a3))
* compact print removes extras spaces ([31cba6c](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/31cba6c3a43ada37b683d6f1011f301d4194f32c))
* object to array function consider nested objects ([59c3081](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/59c3081de1f87aa7b045ea7e6024d41b1d37678a))


### Tests

* **static:** critical fixes for reentrant issues ([5d4941e](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/5d4941ea9b38e25cba7c1b78bdfaad9eb81e44c2))


### CI

* changed vi analyzer image ([05af19d](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/05af19d21bf2b142fc04b159f53b68d44773019e))

### [0.3.1](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/compare/v0.3.0...v0.3.1) (2021-02-25)


### Features

* add funcion for converting json objects array into labview ([#7](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/issues/7)) ([92de14a](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/92de14a8e5c42efee0513325462ddd8aabb93892))
* new function for extracting values from a JSON key ([#8](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/issues/8)) ([3989d1c](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/3989d1cc85458eb52510b4a538dbcc6c698c2b86))


### Tests

* **unit:** adds test for functio extract value from key ([#8](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/issues/8)) ([5b8bbcd](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/5b8bbcdccdd193e2420df2422dae52bb58ab65d7))
* **unit:** adds test for json object array function ([#7](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/issues/7)) ([0c703de](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/0c703deae104f363c048f5807d3dd36374df9965))


### CI

* fix vi analyzer version and cleaned changelog ([3f1bc9b](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/3f1bc9bece06ef76b39bda40cb04ef0eab6054b5))
* included automatic versioning files ([#9](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/issues/9))([@felipefoz](https://gitlab.com/felipefoz)) ([37171dd](https://gitlab.com/felipe_public/labview-shared-libraries/object-to-json/commit/37171ddd3d8f5a40f2d9d706ff648bc80f6dc6f6))
