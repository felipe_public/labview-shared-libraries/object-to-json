# Object to JSON Interface

This library holds a Object to JSON Interface.

## Installation

Download this library into your source code folder.

## Usage
Your class must inherit from this interface and implement the methods:
- Flatten Object to JSON
- Unflatten Object from JSON

![Simple Usage](/docs/snippets/Usage.png)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
BSD3

# Author
Felipe P. Silva